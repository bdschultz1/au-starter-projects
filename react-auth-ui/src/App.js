// src/App.js

import React from "react";
import NavBar from "./components/NavBar";
import { useAuth0 } from "./react-auth0-wrapper";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import Profile from "./components/Profile";
import PrivateRoute from "./components/PrivateRoute";

function App() {
  const { loading, isAuthenticated, getTokenSilently } = useAuth0();
  if (isAuthenticated) {
    getTokenSilently().then(token => console.log(token));
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="App">
      <BrowserRouter>
        <header>
          <NavBar />
        </header>
        <Switch>
          <Route path="/" exact />
          <PrivateRoute path="/profile" component={Profile} />
        </Switch>
      </BrowserRouter>
      {isAuthenticated && <span>Welcome to The Auth App </span>}

      {isAuthenticated && <button onClick={() => getTokenSilently().then(token => console.log(token))}>clicky</button>}

      {isAuthenticated === false && <span>Please login to continue.</span>}
    </div>
  );
}

export default App;
